package com.transport.transport.data.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LatLng {

    Double latitude;

    Double longitude;

}
