package com.transport.transport.data.services;

import com.transport.transport.data.model.Stop;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface StopService {
    @GET("stop")
    Call<List<Stop>> getAll();
}
