package com.transport.transport.data.model;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Request {
    private Long stopId;

    private Long destinationStopId;

    private Long routeId;

    private String status;

    private String creationTime;
}
