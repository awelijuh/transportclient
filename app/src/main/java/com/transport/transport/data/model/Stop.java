package com.transport.transport.data.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Stop {

    private String name;

    private String key;

    private LatLng latLng;

    private Long id;

}
