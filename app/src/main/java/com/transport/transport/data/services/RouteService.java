package com.transport.transport.data.services;

import com.transport.transport.data.model.Route;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RouteService {
    @GET("route")
    Call<List<Route>> getAll();
}
