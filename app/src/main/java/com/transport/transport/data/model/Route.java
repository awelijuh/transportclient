package com.transport.transport.data.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Route {

    private String name;

    private Long id;

}
