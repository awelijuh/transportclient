package com.transport.transport.data.services;

import com.transport.transport.data.model.Request;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RequestService {
    @POST("request")
    Call<Request> save(@Body Request request);
}
