package com.transport.transport.views.interfaces;

import com.transport.transport.data.model.Route;
import com.transport.transport.data.model.Stop;

import java.util.List;

public interface RequestView {

    void setStopList(List<Stop> stopList);

    void setRouteList(List<Route> routeList);

    Stop getStop();

    Stop getDestinationStop();

    Route getRoute();

}
