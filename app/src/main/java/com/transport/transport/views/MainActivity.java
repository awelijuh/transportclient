package com.transport.transport.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.transport.transport.R;
import com.transport.transport.data.model.Route;
import com.transport.transport.data.model.Stop;
import com.transport.transport.presenters.RequestPresenter;
import com.transport.transport.views.interfaces.RequestView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements RequestView {

    private RequestPresenter requestPresenter;

    private StopAdapter stopAdapter1;
    private StopAdapter stopAdapter2;
    private RouteAdapter routeAdapter;

    private Spinner stopSpinner;
    private Spinner destinationStopSpinner;
    private Spinner routeSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPresenter = new RequestPresenter(this);

        stopSpinner = findViewById(R.id.spinner1);
        destinationStopSpinner = findViewById(R.id.spinner2);
        routeSpinner = findViewById(R.id.spinner3);

        stopAdapter1 = initSpinnerStop(stopSpinner);
        stopAdapter2 = initSpinnerStop(destinationStopSpinner);
        routeAdapter = initSpinnerRoute(routeSpinner);

        Button button = findViewById(R.id.buttonSendRequest);
        button.setOnClickListener(v -> {
            requestPresenter.sendRequest();
        });

        requestPresenter.updateData();
    }


    private StopAdapter initSpinnerStop(Spinner spinner) {
        StopAdapter stopAdapter = new StopAdapter(this);
        spinner.setAdapter(stopAdapter);
        return stopAdapter;
    }

    private RouteAdapter initSpinnerRoute(Spinner spinner) {
        RouteAdapter routeAdapter = new RouteAdapter(this);
        spinner.setAdapter(routeAdapter);
        return routeAdapter;
    }


    @Override
    public void setStopList(List<Stop> stopList) {
        stopAdapter1.clear();
        stopAdapter1.addAll(stopList);
        stopAdapter2.clear();
        stopAdapter2.addAll(stopList);
        stopAdapter1.notifyDataSetChanged();
        stopAdapter2.notifyDataSetChanged();
    }

    @Override
    public void setRouteList(List<Route> routeList) {
        routeAdapter.clear();
        routeAdapter.addAll(routeList);
        routeAdapter.notifyDataSetChanged();
    }

    @Override
    public Stop getStop() {
        return (Stop) stopSpinner.getSelectedItem();
    }

    @Override
    public Stop getDestinationStop() {
        return (Stop) destinationStopSpinner.getSelectedItem();
    }

    @Override
    public Route getRoute() {
        return (Route) routeSpinner.getSelectedItem();
    }


    class StopAdapter extends ArrayAdapter<Stop> {

        public StopAdapter(@NonNull Context context) {
            super(context, R.layout.spinner_item);
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.spinner_item, parent, false);
            TextView textView = view.findViewById(R.id.text1);
            textView.setText(getStopName(getItem(position)));
            return view;
        }

        private String getStopName(Stop stop) {
            if (stop == null) {
                return "";
            }
            return stop.getName() + "(" + stop.getKey() + ")";
        }


    }


    class RouteAdapter extends ArrayAdapter<Route> {

        public RouteAdapter(@NonNull Context context) {
            super(context, R.layout.spinner_item);
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.spinner_item, parent, false);
            TextView textView = view.findViewById(R.id.text1);
            textView.setText(getRouteName(getItem(position)));
            return view;
        }

        private String getRouteName(Route route) {
            if (route == null) {
                return "";
            }
            return route.getName();
        }


    }

}
