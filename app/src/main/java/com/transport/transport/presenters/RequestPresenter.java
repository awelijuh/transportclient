package com.transport.transport.presenters;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.transport.transport.data.model.Request;
import com.transport.transport.data.model.Route;
import com.transport.transport.data.model.Stop;
import com.transport.transport.data.services.RequestService;
import com.transport.transport.data.services.RouteService;
import com.transport.transport.data.services.StopService;
import com.transport.transport.views.interfaces.RequestView;

import java.util.List;

import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.transport.transport.data.ServiceGenerator.createService;

@RequiredArgsConstructor
public class RequestPresenter {

    private final RequestView requestView;

    private final RequestService requestService = createService(RequestService.class);
    private final StopService stopService = createService(StopService.class);
    private final RouteService routeService = createService(RouteService.class);

    public void sendRequest() {
        Route route = requestView.getRoute();
        Stop stop = requestView.getStop();
        Stop destinationStop = requestView.getDestinationStop();
        Request request = Request.builder()
                .stopId(stop.getId())
                .destinationStopId(destinationStop != null ? destinationStop.getId() : null)
                .routeId(route != null ? route.getId() : null)
                .build();

        requestService.save(request).enqueue(new Callback<Request>() {
            @Override
            public void onResponse(Call<Request> call, Response<Request> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(((Context) requestView).getApplicationContext(), "Отправлено!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Request> call, Throwable t) {
                Log.d("request_send", t.getMessage());
            }
        });
    }

    public void updateData() {
        stopService.getAll().enqueue(new Callback<List<Stop>>() {
            @Override
            public void onResponse(Call<List<Stop>> call, Response<List<Stop>> response) {
                Log.d("stop-request", "" + response.code());
                if (response.isSuccessful()) {
                    requestView.setStopList(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Stop>> call, Throwable t) {
                Log.d("stop-request", t.getMessage());
            }
        });
        routeService.getAll().enqueue(new Callback<List<Route>>() {
            @Override
            public void onResponse(Call<List<Route>> call, Response<List<Route>> response) {
                if (response.isSuccessful()) {
                    requestView.setRouteList(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Route>> call, Throwable t) {

            }
        });
    }

}
